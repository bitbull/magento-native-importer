<?php
/**
 * Generi setup resource for importers
 *
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
 */
class Dgagliardi_Importer_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{
	/**
	 * Create attribute set.
	 *
     * @param string $setName
     * @param int $copyGroupsFromId
	 * @see Mage_Adminhtml_Catalog_Product_SetController::saveAction().
	 * @return int|boolean
	*/
	function createAttributeSet($setName, $copyGroupsFromId = -1)
	{
		$setName = trim($setName);

		if ($setName == '') {
			return false;
		}

        /* @var $attributeSet Mage_Eav_Model_Entity_Attribute_Set */
		$attributeSet = Mage::getModel('eav/entity_attribute_set');

		// Set the entity type.
		$entityTypeId = $this->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
		$attributeSet->setEntityTypeId($entityTypeId);

		$attributeSet->setAttributeSetName($setName);
		$attributeSet->validate();

		try {
			$attributeSet->save();
		} catch(Exception $ex) {
			Mage::logException($ex);
			return false;
		}

		if (!$attributeSet->getId()) {
			return false;
		}

		if ($copyGroupsFromId == -1) {
			$copyGroupsFromId = $this->getDefaultAttributeSetId($entityTypeId);
		}

		// Load the new set with groups (mandatory). Attach the same groups from the given set-ID to the new set.
		$attributeSet->initFromSkeleton($copyGroupsFromId);

		try {
			$attributeSet->save();
		} catch(Exception $ex) {
			Mage::logException($ex);
			return false;
		}

		return $attributeSet->getId();
	}

    /**
     * Add attribute to an entity type
     *
     * If an attribute_set is specified, attribute will be removed from all sets except this.
     *
     * @param string|integer $entityTypeId
     * @param string $code
     * @param array $attr
     * @return Mage_Eav_Model_Entity_Setup
     */
    public function addAttribute($entityTypeId, $code, array $attr)
    {
        parent::addAttribute($entityTypeId, $code, $attr);

        if (!empty($attr['attribute_set'])) {
            $attributeSetId = $this->getAttributeSetId($entityTypeId, $attr['attribute_set']);

            $select = $this->_conn->select()
                ->from($this->getTable('eav/attribute_set'))
                ->where('attribute_set_id != :attribute_set_id');
            $sets = $this->_conn->fetchAll($select, array('attribute_set_id' => $attributeSetId));
            foreach ($sets as $set) {
                $this->deleteTableRow(
                    'eav/entity_attribute',
                    'attribute_id',
                    $this->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $code),
                    'attribute_set_id',
                    $set['attribute_set_id']
                );
            }
        }

        return $this;
    }
}