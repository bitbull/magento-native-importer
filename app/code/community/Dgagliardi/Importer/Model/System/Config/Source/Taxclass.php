<?php
/**
 * Source for tax class config
 *
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
 */
class Dgagliardi_Importer_Model_System_Config_Source_Taxclass
{
    public function toOptionArray()
    {
        return Mage::getModel('tax/class_source_product')->toOptionArray();
    }
}