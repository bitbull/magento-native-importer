<?php
/**
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
*/
class Dgagliardi_Importer_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param mixed $value
     * @return string
     */
    public function getBooleanValue($value)
    {
        if (in_array($value, array('', 0, null))) {
            return 'No';
        } else {
            return 'Sì';
        }
    }
}