<?php
/**
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
 */
class Dgagliardi_Importer_Helper_Image extends Mage_Core_Helper_Abstract
{
    /**
     * Download image from remote server in given directory.
     *
     * @param string $url
     * @param string $path
     * @return string
    */
    public function downloadImage($url, $path)
    {
        $remoteImage = fopen($url, 'rb');
        $localImage = null;

        if ($remoteImage) {
            $localImage = fopen($path, 'wb');

            if ($localImage) {
                while (!feof($remoteImage)) {
                    fwrite($localImage, fread($remoteImage, 1024 * 8), 1024 * 8);
                }
            }
        }

        if ($remoteImage) {
            fclose($remoteImage);
        }

        if ($localImage) {
            fclose($localImage);
        }

        return $path;
    }

    /**
     * Get the path for product image based on given filename.
     *
     * @param string $filename
     * @return string
    */
    public function getProductImagePath($filename)
    {
        if (strpos($filename, '/') !== false) {
            $filename = substr($filename, strrpos($filename, '/') + 1);
        }

        $filename = Mage_Core_Model_File_Uploader::getCorrectFileName($filename);

        return Mage::getBaseDir('media') . DS . 'import' . DS . $filename;
    }
}