<?php
/**
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
 */
class Dgagliardi_Importer_Helper_Import_Category extends Dgagliardi_Importer_Helper_Import_Abstract
{
    public function __construct()
    {
        parent::__construct();

        // Lista dei processi di index legati alle categorie
        $this->_indexProcesses = array(
            'catalog_category_flat',
            'catalog_url',
            'catalog_category_product'
        );
    }
}