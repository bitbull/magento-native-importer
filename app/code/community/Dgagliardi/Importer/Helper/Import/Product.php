<?php
/**
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
*/
class Dgagliardi_Importer_Helper_Import_Product extends Dgagliardi_Importer_Helper_Import_Abstract
{
    /**
     * Entity to import
     *
     * @var string
     */
    protected $_entity = 'catalog_product';

    /**
     * Name of file imported by Magento
     *
     * @var string
     */
    protected $_importFilename = 'catalog_product.csv';

    /**
     * @var Mage_ImportExport_Model_Import
     */
    protected $_importModel = null;

    /**
     * @var boolean
     */
    protected $_checkStatus = true;

    /**
     * Max memory_limit.
     * Override that set in .htaccess. Change according
     * to your need.
     *
     * @var string
     */
    protected $_memoryLimit = '-1';

    /**
     * @var int
    */
    protected $_mediaAttributeId = null;

    /**
     * Default behavior
     *
     * @var string
    */
    const DEFAULT_BEHAVIOR = 'replace';

    public function __construct()
    {
        parent::__construct();

        // Lista dei processi di index legati ai prodotti
        $this->_indexProcesses = array(
            'catalog_product_price',
            'catalog_category_product',
            'catalogsearch_fulltext',
            'catalog_product_flat',
            'catalog_url',
        );
    }

    /**
     * Run given action against given filename.
     *
     * @param string $action
     * @param string $filename
     * @param string $behavior
     */
    public function run($action, $filename, $behavior = self::DEFAULT_BEHAVIOR)
    {
        $this->_importModel = Mage::getModel('importexport/import');

        // Check is always performed
        $this->_check($filename, $behavior);

        if ('import' == $action) {
            $this->_import();
        }
    }

    /**
     * Execute check agains given file
     *
     * @param string $filename
     * @param string $behavior
     */
    protected function _check($filename, $behavior)
    {
        try {
            $this->_importModel->setData(array(
                'entity'   => $this->_entity,
                'behavior' => $behavior
            ));
            $validationResult = $this->_importModel->validateSource($filename);

            if (!$this->_importModel->getProcessedRowsCount()) {
                $this->_logger->addError($this->__('File does not contain data. Please re-try with another one'));
            } else {
                if (!$validationResult) {
                    if ($this->_importModel->getProcessedRowsCount() == $this->_importModel->getInvalidRowsCount()) {
                        $this->_logger->addNotice(
                            $this->__('File is totally invalid. Please fix errors and re-try')
                        );
                    } elseif ($this->_importModel->getErrorsCount() >= $this->_importModel->getErrorsLimit()) {
                        $this->_logger->addNotice(
                            $this->__('Errors limit (%d) reached. Please fix errors and re-try', $this->_importModel->getErrorsLimit())
                        );
                    } else {
                        if ($this->_importModel->isImportAllowed()) {
                            $this->_logger->addNotice($this->__('File is partially valid, import will be done skipping rows with errors'));
                        } else {
                            $this->_logger->addNotice($this->__('File is partially valid, but import is not possible'));
                        }
                    }
                    // errors info
                    foreach ($this->_importModel->getErrors() as $errorCode => $rows) {
                        $error = $errorCode . ' ' . $this->__('in rows:') . ' ' . implode(', ', $rows);
                        $this->_logger->addError($error);
                    }
                } else {
                    if ($this->_importModel->isImportAllowed()) {
                        $this->_logger->addSuccess($this->__('File is valid!'));
                    } else {
                        $this->_logger->addError($this->__('File is valid, but import is not possible'));
                    }
                }
                $this->_logger->addNotice($this->_importModel->getNotices());
                $this->_logger->addNotice(
                    $this->__(
                        'Checked rows: %d, checked entities: %d, invalid rows: %d, total errors: %d',
                        $this->_importModel->getProcessedRowsCount(), $this->_importModel->getProcessedEntitiesCount(),
                        $this->_importModel->getInvalidRowsCount(), $this->_importModel->getErrorsCount()
                    )
                );
            }

            if ($this->_importModel->isImportAllowed()) {
                $io = new Varien_Io_File();
                $io->cp($filename, $this->getDestinationFilename());

                $this->_checkStatus = true;
            } else {
                $this->_checkStatus = false;
            }
        } catch (Exception $e) {
            $this->_logger->addNotice($this->__('Please fix errors and re-try'))
                ->addError($e->getMessage());

            Mage::logException($e);
        }

        $this->_logger->logMessages();
    }

    protected function _reindex()
    {
        foreach ($this->_indexProcesses as $processCode) {
            $process = Mage::getSingleton('index/indexer')->getProcessByCode($processCode);
            $process->reindexEverything();
        }
    }

    /**
     * Start import
     */
    protected function _import()
    {
        if ($this->_checkStatus) {
            try {
                $this->_importModel->importSource();
                //$this->_importModel->invalidateIndex();
                $this->_reindex();
            } catch (Exception $e) {
                $this->_logger->addError($e->getMessage());
                return;
            }

            $this->_logger->addSuccess($this->__('Import successfully done.'));
        }

        $this->_logger->logMessages();
    }

    public function getDestinationFilename()
    {
        return Mage_ImportExport_Model_Import::getWorkingDir() . $this->_importFilename;
    }

    /**
     * Recuper l'id dell'attributo media_gallery per l'installazione corrente.
     *
     * @return int
    */
    public function getMediaAttributeId()
    {
        if (null === $this->_mediaAttributeId) {
            $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setCodeFilter('media_gallery')
                ->getFirstItem();

            $this->_mediaAttributeId = $attributeInfo->getId();
        }

        return $this->_mediaAttributeId;
    }
}