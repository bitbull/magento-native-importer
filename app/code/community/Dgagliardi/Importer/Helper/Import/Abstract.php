<?php
/**
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
*/
abstract class Dgagliardi_Importer_Helper_Import_Abstract extends Mage_Core_Helper_Abstract
{
    /**
     * Lista dei processi da reindicizzare
     *
     * @var array
    */
    protected $_indexProcesses = array();

    /**
     * Logger
     *
     * @var Dgagliardi_Importer_Helper_Log
    */
    protected $_logger = null;

    /**
     * Inizializza il logger
    */
    public function __construct()
    {
        $this->_logger = Mage::helper('importer/log');
    }
}