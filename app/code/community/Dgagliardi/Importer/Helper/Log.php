<?php
/**
 * @category Dgagliardi
 * @package  Dgagliardi_Importer
 * @author   Gennaro Vietri <gennaro.vietri@gmail.com>
*/
class Dgagliardi_Importer_Helper_Log extends Mage_Core_Helper_Abstract
{
    /**
     * Output messages
     *
     *  @var array
     */
    protected $_messages = array();

    /**
     * @var string
     */
    const LOG_FILENAME = 'importer.log';

    public function addMessage($text, $level)
    {
        $text = is_array($text) ? $text : array($text);

        foreach ($text as $txt) {
            $this->_messages[] = array(
                'level' => $level,
                'text' => $txt
            );
        }
    }

    /**
     * Add notice message
     *
     * @var string
     * @return Dgagliardi_Importer_Helper_Data
     */
    public function addNotice($message)
    {
        $this->addMessage($message, Zend_Log::NOTICE);

        return $this;
    }

    /**
     * Add error message
     *
     * @var string
     * @return Dgagliardi_Importer_Helper_Data
     */
    public function addError($message)
    {
        $this->addMessage($message, Zend_Log::ERR);

        return $this;
    }

    /**
     * Add success message
     *
     * @var string
     * @return Dgagliardi_Importer_Helper_Data
     */
    public function addSuccess($message)
    {
        $this->addMessage($message, Zend_Log::INFO);

        return $this;
    }

    /**
     * Write messages to log file
     */
    public function logMessages()
    {
        if (sizeof($this->_messages) == 0) {
            return;
        }

        foreach ($this->_messages as $message) {
            Mage::log($message['text'], $message['level'], self::LOG_FILENAME);
        }
    }
}